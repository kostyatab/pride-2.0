﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.Entity;
using Pride2.Areas.Admin.Controllers;
using Pride2.Models;
using Xunit;

namespace Pride2.Tests.Admin
{
    public class CategoryControllerTest
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ApplicationDbContext _dbContext;
        public CategoryControllerTest()
        {
            var services = new ServiceCollection();

            services.AddEntityFramework()
                      .AddInMemoryDatabase()
                      .AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());

            _serviceProvider = services.BuildServiceProvider();

            _dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();

            AddData();
        }

        [Theory]
        [InlineData("")]
        [InlineData("s111ubwoofers")]
        public async Task Index_ReturnError(string value)
        {
            // Arrange
            var controller = new CategoryController(_dbContext);

            // Act
            var result = await controller.Index(value);

            // Assert
            Assert.IsType<HttpNotFoundResult>(result);
        }

        [Fact]
        public async Task Index_ReturnCategory()
        {
            // Arrange 
            var controller = new CategoryController(_dbContext);

            // Act
            var result = await controller.Index("subwoofers");

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);

            var resultModel = Assert.IsType<Category>(viewResult.ViewData.Model);
            Assert.NotNull(resultModel.Id);
        }

        private void AddData()
        {
            var categoryList = new List<Category>()
            {
                new Category { Id = 0, Name = "Сабвуферы", Slug = "subwoofers" },
                new Category { Id = 0, Name = "Усилители", Slug = "amplifiers" },
                new Category { Id = 0, Name = "Акустика", Slug = "acoustics" },
                new Category { Id = 0, Name = "Проводка", Slug = "cables" },
                new Category { Id = 0, Name = "Аксессуары", Slug = "accessories" },
                new Category { Id = 0, Name = "Промо-одежда", Slug = "promo" }
            };

            _dbContext.Categories.AddRange(categoryList);

            _dbContext.SaveChanges();
        }
    }
}
