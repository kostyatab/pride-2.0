﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Http.Internal;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.ViewFeatures;
using Microsoft.Data.Entity;
using Microsoft.Extensions.DependencyInjection;
using Pride2.Areas.Admin.Controllers.Dealers;
using Pride2.Areas.Admin.ViewModels;
using Pride2.Models;
using Pride2.Models.Dealers;
using Xunit;

namespace Pride2.Tests.Admin
{
    public class CityControllerTest
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ApplicationDbContext _dbContext;

        public CityControllerTest()
        {
            var services = new ServiceCollection();

            services.AddEntityFramework()
                      .AddInMemoryDatabase()
                      .AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());

            _serviceProvider = services.BuildServiceProvider();
            _dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();

            CreateData();
        }

        [Fact]
        public async Task Create_ReturnView()
        {
            // Arrange
            var controller = new CityController(_dbContext);
            var dbRegion = await _dbContext.ContactsRegions.FirstAsync();

            // Act
            var result = await controller.Create(dbRegion.Id);

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            
            Assert.Equal("Edit", viewResult.ViewName);

            var model = Assert.IsType<CityEditViewModel>(viewResult.ViewData.Model);

            Assert.Equal(dbRegion.Id, model.RegionId);
            
        }

        [Theory]
        [InlineData(999)]
        [InlineData(null)]
        public async Task Create_ReturnError(int? value)
        {
            // Arrange
            var controller = new CityController(_dbContext);

            // Act
            var result = await controller.Create(value);

            //Assert
            Assert.IsType<HttpNotFoundResult>(result);
        }

        [Fact]
        public async Task Create_City()
        {
            // Arrange
            var controller = new CityController(_dbContext);
            controller.ActionContext = new ActionContext()
            {
                HttpContext = new DefaultHttpContext() { Session = new TestSession() }
            };
            controller.TempData = new TempDataDictionary(
                new HttpContextAccessor() { HttpContext = new DefaultHttpContext() { Session = new TestSession() } },
                new SessionStateTempDataProvider());

            var region = await _dbContext.ContactsRegions.FirstOrDefaultAsync();

            var model = new CityEditViewModel()
            {
                City = new City() { Name = "City1" },
                RegionId = region.Id
            };

            // Act
            await controller.Edit(model);

            // Assert
            City city = await _dbContext.ContactsCities.SingleOrDefaultAsync(x => x.Name == "City1");
            Assert.NotNull(city);

        }

        private void CreateData()
        {
            _dbContext.ContactsRegions.Add(new Region()
            {
                Name = "Region Name"
            });

            _dbContext.SaveChanges();
        }
    }
}
