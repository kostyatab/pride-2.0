﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Http.Internal;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.ViewFeatures;
using Microsoft.Data.Entity;
using Microsoft.Extensions.DependencyInjection;
using Pride2.Areas.Admin.Controllers;
using Pride2.Models;
using Xunit;

namespace Pride2.Tests.Admin
{
    public class NewsControllerTest
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ApplicationDbContext _dbContext;

        public NewsControllerTest()
        {
            var services = new ServiceCollection();

            services.AddEntityFramework()
                      .AddInMemoryDatabase()
                      .AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());

            _serviceProvider = services.BuildServiceProvider();
            _dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();

            AddData();
        }

        [Fact]
        public async Task News_Add()
        {
            // Arrange
            var controller = new NewsController(_dbContext, null);

            controller.ActionContext = new ActionContext()
            {
                HttpContext = new DefaultHttpContext() {Session = new TestSession()}
            };
            controller.TempData = new TempDataDictionary(
                new HttpContextAccessor() {HttpContext = new DefaultHttpContext() { Session = new TestSession() } }, 
                new SessionStateTempDataProvider());


            News news = new News()
            {
                Article = "123",
                ShortArticle = "321",
                DateTime = DateTime.Now,
                Info = "qwe",
                Title = "123"
            };

            // Act 
            var result = await controller.Edit(news, null);

            // Assert
            var dbNews = await _dbContext.News.SingleOrDefaultAsync(x => x.Article == "123");

            Assert.NotNull(dbNews);
            Assert.Equal(dbNews.Article, news.Article);
            Assert.Equal(dbNews.ShortArticle, news.ShortArticle);
            Assert.Equal(dbNews.DateTime, news.DateTime);
            Assert.Equal(dbNews.Info, news.Info);
            Assert.Equal(dbNews.Title, news.Title);
        }

        [Fact]
        public async Task News_Edit()
        {
            // Arrange
            var controller = new NewsController(_dbContext, null);

            controller.ActionContext = new ActionContext()
            {
                HttpContext = new DefaultHttpContext() { Session = new TestSession() }
            };
            controller.TempData = new TempDataDictionary(
                new HttpContextAccessor() { HttpContext = new DefaultHttpContext() { Session = new TestSession() } },
                new SessionStateTempDataProvider());


            var news = await _dbContext.News.SingleOrDefaultAsync(x => x.Article == "EditArticle");
            news.Article = "TestArticle_";
            news.ShortArticle = "TestShortArticle_";
            news.Info = "TestInfo_";
            news.Title = "TestTitle_";

            // Act 
            await controller.Edit(news, null);

            // Assert
            var dbNews = await _dbContext.News.SingleOrDefaultAsync(x => x.Article == "TestArticle_");

            Assert.NotNull(dbNews);
            Assert.Equal(dbNews.Article, news.Article);
            Assert.Equal(dbNews.ShortArticle, news.ShortArticle);
            Assert.Equal(dbNews.DateTime, news.DateTime);
            Assert.Equal(dbNews.Info, news.Info);
            Assert.Equal(dbNews.Title, news.Title);
        }

        [Fact]
        public async Task News_Delete()
        {
            // Arrange
            var deleteNews = await _dbContext.News.SingleOrDefaultAsync(x => x.Article == "TestArticle");
            var controller = new NewsController(_dbContext, null);
            controller.ActionContext = new ActionContext()
            {
                HttpContext = new DefaultHttpContext() { Session = new TestSession() }
            };
            controller.TempData = new TempDataDictionary(
                new HttpContextAccessor() {HttpContext = new DefaultHttpContext() {Session = new TestSession()}},
                new SessionStateTempDataProvider());

            // Act
            await controller.Delete(deleteNews.Id);

            // Assert
            var dbDeleteNews = await _dbContext.News.SingleOrDefaultAsync(x => x.Article == "TestArticle");
            Assert.Null(dbDeleteNews);
        }

        private void AddData()
        {
            List<News> newses = new List<News>()
            {
                new News()
                {
                    Article = "TestArticle",
                    ShortArticle = "TestShortArticle",
                    DateTime = DateTime.Now,
                    Info = "TestInfo",
                    Title = "TestTitle"
                },
                new News()
                {
                    Article = "EditArticle",
                    ShortArticle = "EditShortArticle",
                    DateTime = DateTime.Now,
                    Info = "EditInfo",
                    Title = "EditTitle"
                }

            };

            _dbContext.News.AddRange(newses);
            _dbContext.SaveChanges();
        }
    }
}
