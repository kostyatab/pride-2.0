﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Http.Internal;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.ViewFeatures;
using Microsoft.Data.Entity;
using Microsoft.Extensions.DependencyInjection;
using Pride2.Areas.Admin.Controllers;
using Pride2.Models;
using Xunit;

namespace Pride2.Tests.Admin
{
    public class MediaControllerTest
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ApplicationDbContext _dbContext;

        public MediaControllerTest()
        {
            var services = new ServiceCollection();

            services.AddEntityFramework()
                      .AddInMemoryDatabase()
                      .AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());

            _serviceProvider = services.BuildServiceProvider();
            _dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();

            AddData();

        }

        [Fact]
        public async void Media_Add()
        {
            //arrange
            var controller = new MediaController(_dbContext)
            {
                ActionContext = new ActionContext()
                {
                    HttpContext = new DefaultHttpContext() {Session = new TestSession()}
                },
                TempData = new TempDataDictionary(
                    new HttpContextAccessor() {HttpContext = new DefaultHttpContext() {Session = new TestSession()}},
                    new SessionStateTempDataProvider())
            };

            var media = new Media() {Link = "testlink"};

            //act
            await controller.Create(media);

            //assert
            var dbMedia = await _dbContext.Mediae.FirstOrDefaultAsync(x => x.Link == "testlink");

            Assert.NotNull(dbMedia);
        }

        [Fact]
        public async void Media_Delete()
        {
            //arrange
            var controller = new MediaController(_dbContext)
            {
                ActionContext = new ActionContext()
                {
                    HttpContext = new DefaultHttpContext() {Session = new TestSession()}
                },
                TempData = new TempDataDictionary(
                    new HttpContextAccessor() {HttpContext = new DefaultHttpContext() {Session = new TestSession()}},
                    new SessionStateTempDataProvider())
            };

            //act
            await controller.DeleteConfirmed(1);

            //assert
            var media = await _dbContext.Mediae.FirstOrDefaultAsync(x => x.Id == 1);
            Assert.Null(media);
        }

        [Fact]
        public async void Media_Edit()
        {
            //arrange
            var controller = new MediaController(_dbContext)
            {
                ActionContext = new ActionContext()
                {
                    HttpContext = new DefaultHttpContext() {Session = new TestSession()}
                },
                TempData = new TempDataDictionary(
                    new HttpContextAccessor() {HttpContext = new DefaultHttpContext() {Session = new TestSession()}},
                    new SessionStateTempDataProvider())
            };

            var media = await _dbContext.Mediae.FirstOrDefaultAsync(x => x.Link == "link1");

            //act
            media.Link = "editlink1";
            await controller.Edit(media);

            //assert
            var dbMedia = await _dbContext.Mediae.FirstOrDefaultAsync(x => x.Id == media.Id);
            Assert.NotNull(dbMedia);
            Assert.Equal(dbMedia.Link, "editlink1");
        }

        private void AddData()
        {
            var mediaList = new List<Media>()
            {
                new Media() {DateCreate = new DateTime(2016, 3, 21), Link = "link1"},
                new Media() {DateCreate = new DateTime(2016, 3, 22), Link = "link2"}
            };

            _dbContext.Mediae.AddRange(mediaList);
            _dbContext.SaveChanges();
        }
    }
}
