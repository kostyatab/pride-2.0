﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Http.Internal;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.ViewFeatures;
using Microsoft.Data.Entity;
using Microsoft.Extensions.DependencyInjection;
using Pride2.Areas.Admin.Controllers;
using Pride2.Areas.Admin.ViewModels;
using Pride2.Models;
using Xunit;

namespace Pride2.Tests.Admin
{
    public class StockControllerTest
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ApplicationDbContext _dbContext;
        public StockControllerTest()
        {
            var services = new ServiceCollection();

            services.AddEntityFramework()
                      .AddInMemoryDatabase()
                      .AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());

            _serviceProvider = services.BuildServiceProvider();

            _dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();

            AddData();
        }

        [Fact]
        public async Task AddProductToStock_AddItem()
        {
            // Arrange
            var controller = new StockController(_dbContext);
            controller.ActionContext = new ActionContext()
            {
                HttpContext = new DefaultHttpContext() { Session = new TestSession() }
            };
            controller.TempData = new TempDataDictionary(
                new HttpContextAccessor() { HttpContext = new DefaultHttpContext() { Session = new TestSession() } },
                new SessionStateTempDataProvider());

            var product = await _dbContext.Products.SingleOrDefaultAsync(x => x.Id == 1);

            // Act
            await controller.AddProductToStock(product.Id, null);

            // Assert
            var stock = await _dbContext.Stocks.SingleOrDefaultAsync(x => x.Product.Id == 1);
            Assert.NotNull(stock);
        }

        [Fact]
        public async Task AddProductToStock_AddItemWithConfig()
        {
            // Arrange
            var controller = new StockController(_dbContext);
            controller.ActionContext = new ActionContext()
            {
                HttpContext = new DefaultHttpContext() { Session = new TestSession() }
            };
            controller.TempData = new TempDataDictionary(
                new HttpContextAccessor() { HttpContext = new DefaultHttpContext() { Session = new TestSession() } },
                new SessionStateTempDataProvider());

            var product = await _dbContext.Products.SingleOrDefaultAsync(x => x.Id == 3);
            var config = product.ProductConfiguration.ProductConfigurationItem.SingleOrDefault(x => x.Id == 1);

            // Act
            await controller.AddProductToStock(product.Id, config.Id);

            // Assert
            var stock = await _dbContext.Stocks.SingleOrDefaultAsync(x => x.Product.Id == 3);
            Assert.NotNull(stock);
            Assert.Equal(stock.ProductConfigurationItem.Id, 1);
        }

        [Fact]
        public async Task ChangeQuantity_ChangePlus()
        {
            // Arrange
            var controller = new StockController(_dbContext);
            controller.ActionContext = new ActionContext()
            {
                HttpContext = new DefaultHttpContext() { Session = new TestSession() }
            };
            controller.TempData = new TempDataDictionary(
                new HttpContextAccessor() { HttpContext = new DefaultHttpContext() { Session = new TestSession() } },
                new SessionStateTempDataProvider());

            var stockItem = new Stock() { Id = 999, Quantity = 0 };
            _dbContext.Stocks.Add(stockItem);
            await _dbContext.SaveChangesAsync();

            StockChangeQuantityViewModel model = new StockChangeQuantityViewModel() {IsPlus = true, Value = 15, Stock = new Stock() {Id = 999} };

            // Act
            await controller.ChangeQuantity(model);

            // Assert
            var stock = await _dbContext.Stocks.SingleOrDefaultAsync(x => x.Id == 999);
            Assert.NotNull(stock);
            Assert.Equal(stock.Quantity, 15);
        }

        [Fact]
        public async Task ChangeQuantity_ChangeMinus()
        {
            // Arrange
            var controller = new StockController(_dbContext);
            controller.ActionContext = new ActionContext()
            {
                HttpContext = new DefaultHttpContext() { Session = new TestSession() }
            };
            controller.TempData = new TempDataDictionary(
                new HttpContextAccessor() { HttpContext = new DefaultHttpContext() { Session = new TestSession() } },
                new SessionStateTempDataProvider());

            var stockItem = new Stock() { Id = 999, Quantity = 0 };
            _dbContext.Stocks.Add(stockItem);
            await _dbContext.SaveChangesAsync();

            StockChangeQuantityViewModel model = new StockChangeQuantityViewModel() { IsPlus = false, Value = 15, Stock = new Stock() { Id = 999 } };

            // Act
            await controller.ChangeQuantity(model);

            // Assert
            var stock = await _dbContext.Stocks.SingleOrDefaultAsync(x => x.Id == 999);
            Assert.NotNull(stock);
            Assert.Equal(stock.Quantity, 0);
        }

        private void AddData()
        {
            var productList = new List<Product>()
            {
                new Product() {Id = 1, Name = "productName", Price = 100},
                new Product() {Id = 2, Price = 100},
                new Product()
                {
                    Id = 3, Price = 100,
                    ProductConfiguration = new ProductConfiguration()
                    {
                        ProductConfigurationItem = new List<ProductConfigurationItem>()
                        {
                            new ProductConfigurationItem() { Id = 1, Name = "name1"}
                        }
                    }
                }
            };

            _dbContext.Products.AddRange(productList);

            _dbContext.SaveChanges();
        }
    }
}
