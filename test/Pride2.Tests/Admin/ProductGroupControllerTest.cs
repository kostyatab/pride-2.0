﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.Entity;
using Pride2.Areas.Admin.Controllers;
using Pride2.Models;
using Xunit;

namespace Pride2.Tests.Admin
{
    public class ProductGroupControllerTest
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ApplicationDbContext _dbContext;
        public ProductGroupControllerTest()
        {
            var services = new ServiceCollection();

            services.AddEntityFramework()
                      .AddInMemoryDatabase()
                      .AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());

            _serviceProvider = services.BuildServiceProvider();

            _dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();

            AddData();
        }

        [Fact]
        public async Task ChangeSort_Up()
        {
            // Arrange
            var controller =new ProductGroupController(_dbContext);

            // Act
            await controller.ChangeSort(1, true);

            // Assert
            var group = await _dbContext.ProductGroups.SingleOrDefaultAsync(x => x.Id == 1);
            Assert.Equal(group.Sort,  3);
        }

        [Fact]
        public async Task ChangeSort_Down()
        {
            // Arrange
            var controller = new ProductGroupController(_dbContext);

            // Act
            await controller.ChangeSort(4, false);

            // Assert
            var group = await _dbContext.ProductGroups.SingleOrDefaultAsync(x => x.Id == 4);
            Assert.Equal(group.Sort, 3);
        }

        private void AddData()
        {
            var category = new Category() {Id = 1, Name = "category", Slug = "category"};

            _dbContext.Entry<Category>(category).State = EntityState.Added;

            _dbContext.SaveChanges();

            var groupList = new List<ProductGroup>()
            {
                new ProductGroup() {Id = 1, Category = category, Name = "group1", Sort = 1, Products = null},
                new ProductGroup() {Id = 3, Category = category, Name = "group3", Sort = 3, Products = null},
                new ProductGroup() {Id = 4, Category = category, Name = "group4", Sort = 4, Products = null},
            };

            _dbContext.ProductGroups.AddRange(groupList);

            _dbContext.SaveChanges();
        }
    }
}
