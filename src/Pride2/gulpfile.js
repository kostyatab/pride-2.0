/// <binding Clean='clean' />
"use strict";

var gulp = require("gulp"),
  rimraf = require("rimraf"),
  concat = require("gulp-concat"),
  cssmin = require("gulp-cssmin"),
  uglify = require("gulp-uglify"),
  less = require('gulp-less'),
  clean = require('gulp-clean'),
  runSequence = require('run-sequence'),
  plumber = require('gulp-plumber'),
  rename = require('gulp-rename'),
  csso = require('gulp-csso'),
  csscomb = require('gulp-csscomb'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  gulpBowerFiles = require('gulp-bower-files'),
  watch = require('gulp-watch'),
  notify = require("gulp-notify"),
  LessPluginAutoPrefix = require('less-plugin-autoprefix'),
    autoprefixPlugin = new LessPluginAutoPrefix();

var paths = {
  webroot: "./wwwroot/",
  content: "./assets/"
};


paths.js = paths.webroot + "js/";
paths.jsContent = paths.content + "ts/*.js";
paths.css = paths.webroot + "css/**/*.css";
paths.less = paths.content + "less/**/*.less";
paths.img = paths.webroot + "img/";
paths.imgContent = paths.content + "img/**/*.*";
paths.imgContentFolder = paths.content + "img/**/";
paths.appLess = paths.content + "less/app.less";
paths.appCss = paths.webroot + "css/";
paths.appCssName = "app.css";
paths.appJsName = "app.js";
paths.libJsName = "lib.js";
paths.libContent = paths.content + "lib/**/*.js";
paths.libContentFolder = paths.content + "lib/";
paths.lib = paths.webroot + "lib/";

//======================== Clean ========================//

gulp.task("clean:js", function (cb) {
  return gulp.src(paths.js, { read: false })
    .pipe(clean({ force: true }));
});

gulp.task("clean:css", function (cb) {
  return gulp.src(paths.css, { read: false })
  .pipe(clean({ force: true }));
});

gulp.task("clean:img", function (cb) {
  return gulp.src(paths.img, { read: false })
  .pipe(clean({ force: true }));
});


gulp.task("clean", function () {
  runSequence(["clean:css", "clean:js", "clean:img"]);
});

//======================== Clean ========================//

//======================== Style ========================//

gulp.task('styles', function () {
  return gulp.src(paths.appLess)
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(less({ plugins: [autoprefixPlugin] }))
    .pipe(rename(paths.appCssName))
    .pipe(plumber.stop())
    .pipe(gulp.dest(paths.appCss));
});

gulp.task('styles:build', function () {
  return gulp.src(paths.appLess)
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(less({ plugins: [autoprefixPlugin] }))
    .pipe(rename(paths.appCssName))
    .pipe(csscomb())
    .pipe(csso())
    .pipe(gulp.dest(paths.appCss));
});

//======================== Style ========================//

//======================== Image ========================//

gulp.task('copy:img', function () {
  return gulp.src(paths.imgContent)
      .pipe(gulp.dest(paths.img));
});

gulp.task('copy:img-build', function () {
  return gulp.src(paths.imgContent)
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{ removeViewBox: false }],
      use: [pngquant()]
    }))
    .pipe(gulp.dest(paths.img));
});

//======================== Image ========================//

//======================== Scripts ========================//

gulp.task('scripts-app', function () {
  return gulp.src(paths.jsContent)
      .pipe(concat(paths.appJsName))
      .pipe(gulp.dest(paths.js));
});

gulp.task('scripts-app:build', function () {
  return gulp.src(paths.jsContent)
      .pipe(concat(paths.appJsName))
      .pipe(uglify())
      .pipe(gulp.dest(paths.js));
});

gulp.task('scripts-plugins', function () {
  return gulp.src(paths.libContent)
      .pipe(concat(paths.libJsName))
      .pipe(gulp.dest(paths.lib));
});

gulp.task('scripts-plugins:build', function () {
  return gulp.src(paths.libContent)
      .pipe(concat(paths.libJsName))
      .pipe(uglify())
      .pipe(gulp.dest(paths.lib));
});

//gulp.task("bower-files", function () {
//  gulpBowerFiles().pipe(gulp.dest(paths.libContentFolder));
//});

gulp.task("scripts", function () {
  runSequence(["scripts-app", "scripts-plugins"]);
});

gulp.task("scripts:build", function () {
  runSequence(["scripts-app:build", "scripts-plugins:build"]);
});

//======================== Scripts ========================//

//======================== Watcher ========================//

gulp.task('watch:js-app', function () {
  watch(paths.jsContent, function () {
    gulp.start('scripts-app');
  });
});

gulp.task('watch:js-lib', function () {
  watch(paths.libContent, function () {
    gulp.start('scripts-plugins');
  });
});

gulp.task('watch:less', function () {
  watch(paths.less, function () {
    gulp.start('styles');
  });
});

gulp.task('watch:img', function () {
  watch(paths.imgContentFolder + '*.jpg', function () {
    gulp.start('copy:img');
  });
  watch(paths.imgContentFolder + '*.png', function () {
    gulp.start('copy:img');
  });
  watch(paths.imgContentFolder + '*.gif', function () {
    gulp.start('copy:img');
  });
});

gulp.task("watch", function () {
  runSequence(["watch:js-app", "watch:js-lib", "watch:less", "watch:img"]);
});

//======================== Watcher ========================//

gulp.task('build', function () {
  runSequence('clean', ['styles:build', 'copy:img-build', 'scripts:build']);
});

gulp.task('default', function () {
  runSequence(['styles', 'copy:img', 'scripts', 'watch']);
});