﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Pride2.Areas.Admin.ViewModels;
using Pride2.Models;

namespace Pride2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin/product/configuration/item")]
    public class ProductConfigurationItemController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProductConfigurationItemController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Route("create")]
        public async Task<IActionResult> Create(int? configurationId)
        {
            if (configurationId != null)
            {
                var property = await _context.ProductConfigurations.FirstOrDefaultAsync(x => x.Id == configurationId.Value);

                if (property != null)
                {
                    ProductConfigurationItemViewModel model = new ProductConfigurationItemViewModel()
                    {
                        ProductConfigurationItem = new ProductConfigurationItem(),
                        ConfigurationId = configurationId.Value
                    };

                    return View("Edit", model);
                }
            }

            return HttpNotFound();
        }

        [Route("edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ProductConfigurationItem configurationItem = await _context.ProductConfigurationItems.Include(x => x.ProductConfiguration).SingleAsync(x => x.Id == id);

            if (configurationItem != null)
            {
                ProductConfigurationItemViewModel model = new ProductConfigurationItemViewModel()
                {
                    ProductConfigurationItem = configurationItem,
                    ConfigurationId = configurationItem.ProductConfiguration.Id
                };

                return View(model);
            }

            return HttpNotFound();
        }

        [HttpPost("edit")]
        public async Task<IActionResult> Edit(ProductConfigurationItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.ProductConfigurationItem.ProductConfiguration = await _context.ProductConfigurations
                    .Include(x => x.Product)
                    .SingleOrDefaultAsync(x => x.Id == model.ConfigurationId);

                if (model.ProductConfigurationItem.ProductConfiguration == null)
                {
                    return HttpNotFound();
                }

                if (model.ProductConfigurationItem.Id == 0)
                {
                    _context.Entry(model.ProductConfigurationItem).State = EntityState.Added;

                    TempData["Message"] = $"Property item: {model.ProductConfigurationItem.Name} create!";
                }
                else
                {
                    _context.Entry(model.ProductConfigurationItem).State = EntityState.Modified;

                    TempData["Message"] = $"Property item: {model.ProductConfigurationItem.Name} save!";
                }

                await _context.SaveChangesAsync();

                return RedirectToAction("Edit", "Product", new { id = model.ProductConfigurationItem.ProductConfiguration.Product.Id });
            }

            return View(model);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete(int id)
        {
            ProductConfigurationItem configurationItem = await _context.ProductConfigurationItems
                .Include(x => x.ProductConfiguration)
                .ThenInclude(x => x.Product)
                .SingleAsync(x => x.Id == id);

            if (configurationItem != null)
            {
                _context.ProductConfigurationItems.Remove(configurationItem);

                TempData["Message"] = $"Property item: {configurationItem.Name} delete!";

                await _context.SaveChangesAsync();

                return RedirectToAction("Edit", "Product", new { configurationItem.ProductConfiguration.Product.Id });
            }

            return HttpNotFound();
        }
    }
}
