using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Pride2.Models;

namespace Pride2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin/media")]
    public class MediaController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MediaController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Media
        public async Task<IActionResult> Index()
        {
            return View(await _context.Mediae.OrderByDescending(x => x.DateCreate).ToListAsync());
        }

        // GET: Media/Create
        [Route("create")]
        public IActionResult Create()
        {
            return View(new Media());
        }

        // POST: Media/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("create")]
        public async Task<IActionResult> Create(Media media)
        {
            if (ModelState.IsValid)
            {
                media.DateCreate = DateTime.Now;
                _context.Mediae.Add(media);
                await _context.SaveChangesAsync();

                TempData["Message"] = $"Media item create!";
                return RedirectToAction("Index");
            }
            return View(media);
        }

        // GET: Media/Edit/5
        [Route("edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Media media = await _context.Mediae.SingleAsync(m => m.Id == id);
            if (media == null)
            {
                return HttpNotFound();
            }
            return View(media);
        }

        // POST: Media/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("edit")]
        public async Task<IActionResult> Edit(Media media)
        {
            if (ModelState.IsValid)
            {
                _context.Update(media);
                await _context.SaveChangesAsync();

                TempData["Message"] = $"Media item save!";
                return RedirectToAction("Index");
            }
            return View(media);
        }

        // POST: Media/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Route("delete")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var media = await _context.Mediae.FirstOrDefaultAsync(m => m.Id == id);

            if (media == null)
            {
                return HttpBadRequest();
            }

            _context.Mediae.Remove(media);
            await _context.SaveChangesAsync();

            TempData["Message"] = $"Media item delete!";
            return RedirectToAction("Index");
        }
    }
}
