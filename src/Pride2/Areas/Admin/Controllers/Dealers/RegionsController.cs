using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Pride2.Models;
using Pride2.Models.Dealers;

namespace Pride2.Areas.Admin.Controllers.Dealers
{
    [Area("admin")]
    [Route("admin/dealers/regions")]
    public class RegionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public RegionsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.ContactsRegions.Include(x => x.Cities).ToListAsync());
        }

        [Route("create")]
        public IActionResult Create()
        {
            return View("Edit", new Region());
        }

        [Route("edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Region region = await _context.ContactsRegions.SingleAsync(m => m.Id == id);
            if (region == null)
            {
                return HttpNotFound();
            }
            return View(region);
        }

        [HttpPost("edit")]
        public async Task<IActionResult> Edit(Region region)
        {
            if (ModelState.IsValid)
            {

                if (region.Id == 0)
                {
                    _context.Entry(region).State = EntityState.Added;

                    TempData["Message"] = $"Region: {region.Name} create!";
                }
                else
                {
                    _context.Entry(region).State = EntityState.Modified;

                    TempData["Message"] = $"Region: {region.Name} save!";
                }

                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return View(region);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete(int id)
        {
            Region region = await _context.ContactsRegions.Include(x => x.Cities).SingleAsync(m => m.Id == id);
            _context.ContactsCities.RemoveRange(region.Cities);
            _context.ContactsRegions.Remove(region);
            await _context.SaveChangesAsync();

            TempData["Message"] = $"Region: {region.Name} delete!";

            return RedirectToAction("Index");
        }
    }
}
