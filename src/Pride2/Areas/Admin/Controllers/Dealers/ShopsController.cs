using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Pride2.Areas.Admin.ViewModels;
using Pride2.Models;
using Pride2.Models.Dealers;

namespace Pride2.Areas.Admin.Controllers.Dealers
{
    [Area("admin")]
    [Route("admin/dealers/shop")]
    public class ShopsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ShopsController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Route("create")]
        public async Task<IActionResult> Create(int? dealerId)
        {
            if (dealerId != null)
            {
                var dealer = await _context.Dealers.SingleAsync(x => x.Id == dealerId);

                if (dealer != null)
                {
                    ShopEditViewModel model = new ShopEditViewModel()
                    {
                        Shop = new Shop(),
                        Cities = await _context.ContactsCities.ToListAsync(),
                        DealerId = dealer.Id
                    };

                    return View("Edit" , model);
                }
            }

            return HttpNotFound();
        }

        [Route("edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Shop shop = await _context.Shops.Include(x => x.Dealer).SingleAsync(x => x.Id == id);

            if (shop != null)
            {
                ShopEditViewModel model = new ShopEditViewModel()
                {
                    Shop = shop,
                    Cities = await _context.ContactsCities.ToListAsync(),
                    DealerId = shop.Dealer.Id
                };

                return View(model);
            }

            return HttpNotFound();
        }

        [HttpPost("edit")]
        public async Task<IActionResult> Edit(ShopEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Shop.City = await _context.ContactsCities.SingleAsync(x => x.Id == model.CityId);
                model.Shop.Dealer = await _context.Dealers.SingleAsync(x => x.Id == model.DealerId);

                if (model.Shop.City == null || model.Shop.Dealer == null)
                {
                    return HttpNotFound();
                }

                if (model.Shop.Id == 0)
                {
                    _context.Entry(model.Shop).State = EntityState.Added;

                    TempData["Message"] = $"Shop: {model.Shop.Name} create!";
                }
                else
                {
                    _context.Entry(model.Shop).State = EntityState.Modified;

                    TempData["Message"] = $"Shop: {model.Shop.Name} save!";
                }

                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Dealer");
            }

            return View(model);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete(int id)
        {
            Shop shop = await _context.Shops.SingleAsync(x => x.Id == id);

            if (shop != null)
            {
                _context.Shops.Remove(shop);

                TempData["Message"] = $"Shop: {shop.Name} delete!";
            }

            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Dealer");
        }
    }
}