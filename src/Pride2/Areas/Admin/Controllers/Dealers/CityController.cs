using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Pride2.Areas.Admin.ViewModels;
using Pride2.Models;
using Pride2.Models.Dealers;

namespace Pride2.Areas.Admin.Controllers.Dealers
{
    [Area("admin")]
    [Route("admin/dealers/city")]
    public class CityController : Controller
    {
        private ApplicationDbContext _context;

        public CityController(ApplicationDbContext context)
        {
            _context = context;    
        }

        [Route("create")]
        public async Task<IActionResult> Create(int? regionId)
        {
            if (regionId != null)
            {
                var region = await _context.ContactsRegions.SingleOrDefaultAsync(x => x.Id == regionId);

                if (region != null)
                {
                    CityEditViewModel model = new CityEditViewModel()
                    {
                        City = new City(),
                        RegionId = region.Id
                    };

                    return View("Edit", model);
                }
            }

            return HttpNotFound();
        }

        [Route("edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            City city = await _context.ContactsCities.Include(x => x.Region).SingleAsync(x => x.Id == id);

            if (city != null)
            {
                CityEditViewModel model = new CityEditViewModel()
                {
                    City = city,
                    RegionId = city.Region.Id
                };

                return View(model);
            }

            return HttpNotFound();
        }

        [HttpPost("edit")]
        public async Task<IActionResult> Edit(CityEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.City.Region = await _context.ContactsRegions.SingleOrDefaultAsync(x => x.Id == model.RegionId);

                if (model.City.Region == null)
                {
                    return HttpNotFound();
                }

                if (model.City.Id == 0)
                {
                    _context.Entry(model.City).State = EntityState.Added;

                    TempData["Message"] = $"City: {model.City.Name} create!";
                }
                else
                {
                    _context.Entry(model.City).State = EntityState.Modified;

                    TempData["Message"] = $"City: {model.City.Name} save!";
                }

                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Regions");
            }

            return View(model);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete(int id)
        {
            City city = await _context.ContactsCities.SingleAsync(x => x.Id == id);

            if (city != null)
            {
                _context.ContactsCities.Remove(city);

                TempData["Message"] = $"City: {city.Name} delete!";
            }

            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Regions");
        }
    }
}
