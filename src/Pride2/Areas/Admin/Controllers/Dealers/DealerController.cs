﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.ChangeTracking;
using Pride2.Models;
using Pride2.Models.Dealers;

namespace Pride2.Areas.Admin.Controllers.Dealers
{
    [Area("admin")]
    [Route("admin/dealers")]
    public class DealerController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DealerController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {

            var dialerList = await _context.Dealers.Include(x => x.Shops).ThenInclude(x => x.City).ToListAsync();

            return View(dialerList);
        }

        [Route("create")]
        public IActionResult Create()
        {
            var dealer = new Dealer();

            return View(nameof(Edit), dealer);
        }

        [HttpGet("edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Dealer dealer = await _context.Dealers.SingleAsync(x => x.Id == id);

            if (dealer != null)
            {
                return View(dealer);
            }

            return HttpNotFound();
        }

        [HttpPost("edit")]
        public async Task<IActionResult> Edit(Dealer dealer)
        {
            if (ModelState.IsValid)
            {

                if (dealer.Id == 0)
                {
                    _context.Entry(dealer).State = EntityState.Added;

                    TempData["Message"] = $"Dealer: {dealer.Name} {dealer.Surname} create!";
                }
                else
                {
                    _context.Entry(dealer).State = EntityState.Modified;

                    TempData["Message"] = $"Dealer: {dealer.Name} {dealer.Surname} save!";
                }

                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return View(dealer);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete(int id)
        {
            Dealer dealer = await _context.Dealers.Include(x => x.Shops).SingleAsync(x => x.Id == id);

            if (dealer != null)
            {
                _context.Dealers.Remove(dealer);

                await _context.SaveChangesAsync();

                TempData["Message"] = $"Dealer: {dealer.Name} {dealer.Surname} delete!";
            }
            else
            {
                TempData["ErrorMessage"] = "Dealer not found!";
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
