﻿using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Pride2.Areas.Admin.ViewModels;
using Pride2.Models;

namespace Pride2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin/product/configuration")]
    public class ProductConfigurationController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProductConfigurationController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Route("create")]
        public async Task<IActionResult> Create(int? productId)
        {
            if (productId != null)
            {
                var product = await _context.Products.Include(x => x.ProductConfiguration).FirstOrDefaultAsync(x => x.Id == productId);

                if (product != null && product.ProductConfiguration == null)
                {
                    ProductConfigurationEditViewModel model = new ProductConfigurationEditViewModel()
                    {
                        ProductConfiguration = new ProductConfiguration(),
                        ProductId = product.Id
                    };

                    return View("Edit", model);
                }
            }

            return HttpNotFound();
        }

        [Route("edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ProductConfiguration productConfiguration = await _context.ProductConfigurations.Include(x => x.Product).SingleAsync(x => x.Id == id);

            if (productConfiguration != null)
            {
                ProductConfigurationEditViewModel model = new ProductConfigurationEditViewModel()
                {
                    ProductConfiguration = productConfiguration,
                    ProductId = productConfiguration.Product.Id
                };

                return View(model);
            }

            return HttpNotFound();
        }

        [HttpPost("edit")]
        public async Task<IActionResult> Edit(ProductConfigurationEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.ProductConfiguration.Product = await _context.Products.SingleOrDefaultAsync(x => x.Id == model.ProductId);

                if (model.ProductConfiguration.Product == null)
                {
                    return HttpNotFound();
                }

                if (model.ProductConfiguration.Id == 0)
                {
                    _context.Entry(model.ProductConfiguration).State = EntityState.Added;

                    TempData["Message"] = $"Property: {model.ProductConfiguration.Name} create!";
                }
                else
                {
                    _context.Entry(model.ProductConfiguration).State = EntityState.Modified;

                    TempData["Message"] = $"Property: {model.ProductConfiguration.Name} save!";
                }

                await _context.SaveChangesAsync();

                return RedirectToAction("Edit", "Product", new {id = model.ProductId});
            }

            return View(model);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete(int id)
        {
            ProductConfiguration productConfiguration = await _context.ProductConfigurations
                .Include(x => x.ProductConfigurationItem)
                .Include(x => x.Product)
                .SingleAsync(x => x.Id == id);

            if (productConfiguration != null)
            {
                _context.ProductConfigurationItems.RemoveRange(productConfiguration.ProductConfigurationItem);
                _context.ProductConfigurations.Remove(productConfiguration);

                TempData["Message"] = $"Property: {productConfiguration.Name} delete!";

                await _context.SaveChangesAsync();

                return RedirectToAction("Edit", "Product", new { productConfiguration.Product.Id});
            }

            return HttpNotFound();
        }
    }
}
