﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Pride2.Areas.Admin.Models;
using Pride2.Models;

namespace Pride2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin/news")]
    public class NewsController: Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;

        public NewsController(ApplicationDbContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        public async Task<IActionResult> Index()
        {
            List<News> listNews = await _context.News.OrderByDescending(x => x.DateTime).ToListAsync();

            return View(listNews);
        }

        [Route("create")]
        public IActionResult Create()
        {
             return View("Edit", new News());
        }

        [Route("edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            News news = await _context.News.SingleOrDefaultAsync(x => x.Id == id);

            if (news != null)
            {
                return View(news);
            }

            return HttpNotFound();
        }

        [HttpPost("edit")]
        public async Task<IActionResult> Edit(News news, IFormFile image)
        {
            if (news.Article == null)
            {
                ModelState.AddModelError(string.Empty, "Article does not allow nulls");
            }

            if (ModelState.IsValid)
            {

                if (news.Id == 0)
                {
                    news.DateTime = DateTime.Now;
                    _context.Entry(news).State = EntityState.Added;

                    TempData["Message"] = $"News: {news.Title} create!";
                }
                else
                {
                    _context.Entry(news).State = EntityState.Modified;

                    TempData["Message"] = $"News: {news.Title} save!";
                }

                var imageUploader = new ImageUploader(_environment, image, "news");
                if (imageUploader.GetFileName() != string.Empty)
                {
                    news.Image = imageUploader.GetFileName();
                }
                
                
                await _context.SaveChangesAsync();

                imageUploader.FolderId = news.Id.ToString();
                await imageUploader.Upload();

                return RedirectToAction("Index");
            }

            return View(news);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete(int id)
        {
            News news = await _context.News.SingleOrDefaultAsync(x => x.Id == id);

            if (news != null)
            {
                _context.News.Remove(news);

                TempData["Message"] = $"News: {news.Title} delete!";
            }

            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}
