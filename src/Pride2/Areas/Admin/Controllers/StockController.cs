﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Pride2.Areas.Admin.ViewModels;
using Pride2.Models;

namespace Pride2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin/stock")]
    public class StockController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StockController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await GetStockItemsAsync());
        }

        [Route("productslist")]
        public async Task<IActionResult> ProductsList()
        {
            return View(await GetProductItemsAsync());
        }

        [HttpPost("add-product-to-stock")]
        public async Task<IActionResult> AddProductToStock(int id, int? configId)
        {
            var dbProduct = await _context.Products.Include(x => x.Stocks).SingleOrDefaultAsync(x => x.Id == id);

            if (dbProduct != null)
            {
                if (configId == null)
                {
                    if (dbProduct.Stocks.Count == 0)
                    {
                        var temp = new Stock() {Product = dbProduct, Quantity = 0};
                        _context.Stocks.Add(temp);

                        await _context.SaveChangesAsync();

                        TempData["Message"] = $"Product: {dbProduct.Name} add to stock!";
                        return RedirectToAction("ProductsList");
                    }
                }
                else
                {
                    var dbConfigProductItem =
                        await _context.ProductConfigurationItems.SingleOrDefaultAsync(x => x.Id == configId.Value);

                    if (dbConfigProductItem != null)
                    {
                        _context.Stocks.Add(new Stock()
                        {
                            Product = dbProduct,
                            ProductConfigurationItem = dbConfigProductItem,
                            Quantity = 0
                        });

                        await _context.SaveChangesAsync();

                        TempData["Message"] = $"Product: {dbProduct.Name} ({dbConfigProductItem.Name}) add to stock!";
                        return RedirectToAction("ProductsList");
                    }
                }
            }


            return HttpNotFound();
        }

        [Route("change-quantity")]
        public async Task<IActionResult> ChangeQuantity(int id, bool plus)
        {
            var dbStock = await _context.Stocks.SingleOrDefaultAsync(x => x.Id == id);

            if (dbStock != null)
            {
                return View(new StockChangeQuantityViewModel() {Stock = dbStock, Value = 0, IsPlus = plus});
            }

            return HttpNotFound();
        }

        [HttpPost("change-quantity")]
        public async Task<IActionResult> ChangeQuantity(StockChangeQuantityViewModel model)
        {
            if (ModelState.IsValid)
            {
                var dbStock = await _context.Stocks.SingleOrDefaultAsync(x => x.Id == model.Stock.Id);

                if (dbStock != null)
                {
                    if (model.IsPlus)
                    {
                        dbStock.Quantity += model.Value;
                    }
                    else
                    {
                        dbStock.Quantity -= model.Value;

                        if (dbStock.Quantity < 0) dbStock.Quantity = 0;
                    }

                    await _context.SaveChangesAsync();

                    return RedirectToAction("Index");
                }

                return HttpNotFound();
            }
            return View(model);
        }

        private async Task<IEnumerable<IGrouping<string, Stock>>> GetStockItemsAsync()
        {
            return await Task.Run(() =>
            {
                var model = _context.Stocks
                    .Include(x => x.Product)
                    .ThenInclude(x => x.Category)
                    .Include(x => x.ProductConfigurationItem)
                    .ToList()
                    .GroupBy(x => x.Product.Category.Name);

                return model;
            });
        }

        private async Task<IEnumerable<IGrouping<string, Product>>> GetProductItemsAsync()
        {
            return await Task.Run(() =>
            {
                return _context.Products
                    .Include(x => x.Stocks)
                    .Include(x => x.Category)
                    .Include(x => x.ProductConfiguration)
                    .ThenInclude(x => x.ProductConfigurationItem)
                    .ToList()
                    .GroupBy(x => x.Category.Name);
            });
        }
    }
}
