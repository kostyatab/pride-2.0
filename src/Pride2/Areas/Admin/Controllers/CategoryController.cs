﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Pride2.Models;

namespace Pride2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin/category")]
    public class CategoryController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CategoryController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Route("{slug}")]
        public async Task<IActionResult> Index(string slug)
        {
            if (slug != string.Empty)
            {
                var category = _context.Categories
                    .Include(x => x.ProductGroup)
                    .ThenInclude(x => x.Products)
                    .ToList()
                    .Where(x => x.Slug == slug)
                    .Select(cat => new Category()
                    {
                        Id = cat.Id,
                        Products = cat.Products,
                        Name = cat.Name,
                        Slug = cat.Slug,
                        ProductGroup = cat.ProductGroup.OrderByDescending(x => x.Sort).ToList()
                    })
                    .FirstOrDefault();
                    

                if (category != null)
                {
                    return View(category);
                }
            }

            return HttpNotFound();
        }
    }
}
