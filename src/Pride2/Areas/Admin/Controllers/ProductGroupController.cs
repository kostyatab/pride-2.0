﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Pride2.Areas.Admin.ViewModels;
using Pride2.Models;

namespace Pride2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin/productgroup")]
    public class ProductGroupController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProductGroupController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Route("create")]
        public IActionResult Create(int categoryId)
        {
            var model = new ProductGroupEditViewModel()
            {
                ProductGroup = new ProductGroup(),
                CategoryId = categoryId
            };

            return View("Edit", model);
        }

        [Route("edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ProductGroup productGroup = await _context.ProductGroups.Include(x => x.Category).SingleOrDefaultAsync(x => x.Id == id);

            if (productGroup != null)
            {
                var model = new ProductGroupEditViewModel()
                {
                    ProductGroup = productGroup,
                    CategoryId = productGroup.Category.Id
                };
                return View(model);
            }

            return HttpNotFound();
        }

        [HttpPost("edit")]
        public async Task<IActionResult> Edit(ProductGroupEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var dbCategory = await _context.Categories.FirstOrDefaultAsync(x => x.Id == model.CategoryId);

                if (dbCategory == null)
                {
                    return HttpNotFound();
                }

                model.ProductGroup.Category = dbCategory;

                if (model.ProductGroup.Id == 0)
                {
                    model.ProductGroup.Sort = await _context.ProductGroups.MaxAsync(x => x.Sort) + 1;

                    _context.Entry(model.ProductGroup).State = EntityState.Added;

                    TempData["Message"] = $"Product Group: {model.ProductGroup.Name} create!";
                }
                else
                {
                    _context.Entry(model.ProductGroup).State = EntityState.Modified;

                    TempData["Message"] = $"Product Group: {model.ProductGroup.Name} save!";
                }

                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Category", new {slug = dbCategory.Slug});
            }

            return View(model);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete(int id)
        {
            var productGroup = await _context.ProductGroups.Include(x => x.Category).SingleOrDefaultAsync(x => x.Id == id);

            if (productGroup != null)
            {
                _context.ProductGroups.Remove(productGroup);

                TempData["Message"] = $"Product group: {productGroup.Name} delete!";

                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Category", new {slug = productGroup.Category.Slug});
            }

            return HttpNotFound();
        }

        [HttpPost("change-sort")]
        public async Task<IActionResult> ChangeSort(int id, bool plus)
        {
            var group = await _context.ProductGroups.Include(x => x.Category).SingleOrDefaultAsync(x => x.Id == id);

            if (group != null)
            {
                if (plus)
                {
                    var nextGroup = _context.ProductGroups
                        .Where(x => x.Sort > group.Sort)
                        .OrderBy(x => x.Sort)
                        .FirstOrDefault();

                    if (nextGroup != null)
                    {
                        int tempSort = nextGroup.Sort;

                        nextGroup.Sort = group.Sort;
                        group.Sort = tempSort;

                        await _context.SaveChangesAsync();
                    }
                }
                else
                {
                    var prevGroup = _context.ProductGroups
                        .Where(x => x.Sort < group.Sort)
                        .OrderByDescending(x => x.Sort)
                        .FirstOrDefault();

                    if (prevGroup != null)
                    {
                        int tempSort = prevGroup.Sort;

                        prevGroup.Sort = group.Sort;
                        group.Sort = tempSort;

                        await _context.SaveChangesAsync();
                    }
                }

                return RedirectToAction("Index", "Category", new { slug  = group.Category.Slug});
            }

            return HttpNotFound();
        }
    }
}
