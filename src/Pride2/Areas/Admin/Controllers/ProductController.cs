﻿using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Pride2.Models;
using System.Threading.Tasks;
using Pride2.Areas.Admin.ViewModels;


namespace Pride2.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin/product")]
    public class ProductController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ProductController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Route("create")]
        public async Task<IActionResult> Create(int? categoryId, int? groupId)
        {
            if (categoryId != null || groupId != null)
            {
                var category = await _context.Categories.FirstOrDefaultAsync(x => x.Id == categoryId.Value);
                var group = await _context.ProductGroups.FirstOrDefaultAsync(x => x.Id == groupId.Value);

                if (category != null)
                {
                    var model = new ProductEditViewModel()
                    {
                        Product = new Product(),
                        CategoryId = category.Id,
                        GroupId = group.Id
                    };

                    return View("Edit", model);
                }
            }

            return HttpNotFound();
        }

        [Route("edit")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            var product = await _context.Products
                .Include(x => x.Category)
                .Include(x => x.ProductGroup)
                .Include(x => x.ProductConfiguration)
                .ThenInclude(x => x.ProductConfigurationItem)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (product != null)
            {
                return View(new ProductEditViewModel()
                {
                    Product = product,
                    CategoryId = product.Category.Id,
                    GroupId = product.ProductGroup.Id
                });
            }

            return HttpNotFound();
        }

        [HttpPost("edit")]
        public async Task<IActionResult> Edit(ProductEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var category = await _context.Categories.FirstOrDefaultAsync(x => x.Id == model.CategoryId);

                if (category == null)
                {
                    return HttpNotFound();
                }

                model.Product.Category = category;
                model.Product.ProductGroup = await _context.ProductGroups.FirstOrDefaultAsync(x => x.Id == model.GroupId);

                if (model.Product.Id == 0)
                {
                    _context.Entry(model.Product).State = EntityState.Added;

                    TempData["Message"] = $"Product: {model.Product.Name} create!";
                }
                else
                {
                    _context.Entry(model.Product).State = EntityState.Modified;

                    TempData["Message"] = $"Product: {model.Product.Name} save!";
                }

                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Category", new {slug = category.Slug});
            }

            return View(model);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete(int id)
        {
            Product product = await _context.Products.Include(x => x.Category).SingleAsync(x => x.Id == id);

            if (product != null)
            {
                _context.Products.Remove(product);

                TempData["Message"] = $"Product: {product} delete!";

                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Category", new {slug = product.Category.Slug});
            }

            return HttpNotFound();
        }
    }
}
