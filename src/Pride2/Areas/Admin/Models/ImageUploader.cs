﻿using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;

namespace Pride2.Areas.Admin.Models
{
    public class ImageUploader
    {
        private readonly IHostingEnvironment _env;
        private readonly IFormFile _image;
        private readonly string _folderName;

        public ImageUploader(IHostingEnvironment env, IFormFile image, string folderName)
        {
            _env = env;
            _image = image;
            _folderName = folderName;
        }

        public string FolderId { get; set; }

        public async Task<string> Upload()
        {
            string fileName = string.Empty;

            if (_env != null)
            {
                var uploadPath = Path.Combine(_env.WebRootPath, "upload", _folderName, FolderId);
                Directory.CreateDirectory(uploadPath);

                if (_image != null && _image.Length > 0)
                {
                    fileName = ContentDispositionHeaderValue.Parse(_image.ContentDisposition).FileName.Trim('"');
                    await _image.SaveAsAsync(Path.Combine(uploadPath, fileName));
                }
            }

            return fileName;
        }

        public string GetFileName()
        {
            string fileName = string.Empty;

            if (_image != null && _image.Length > 0)
            {
                fileName = ContentDispositionHeaderValue.Parse(_image.ContentDisposition).FileName.Trim('"');
            }

            return fileName;
        }
    }
}

