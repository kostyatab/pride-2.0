﻿using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using Pride2.Models;

namespace Pride2.Areas.Admin.ViewComponents
{
    [ViewComponent(Name = "CategoryList")]
    public class CategoryListViewComponent: ViewComponent
    {
        private readonly ApplicationDbContext _context;

        public CategoryListViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var categories = await _context.Categories.ToListAsync();

            return View(categories);
        }
    }
}
