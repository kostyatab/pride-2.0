﻿using Pride2.Models;

namespace Pride2.Areas.Admin.ViewModels
{
    public class ProductEditViewModel
    {
        public Product Product { get; set; }
        public int CategoryId { get; set; }
        public int GroupId { get; set; }
    }
}
