﻿using Pride2.Models;

namespace Pride2.Areas.Admin.ViewModels
{
    public class ProductConfigurationItemViewModel
    {
        public ProductConfigurationItem ProductConfigurationItem{ get; set; }
        public int ConfigurationId { get; set; }
    }
}
