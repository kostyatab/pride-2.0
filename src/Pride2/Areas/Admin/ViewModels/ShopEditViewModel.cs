﻿using System.Collections.Generic;
using System.ComponentModel;
using Pride2.Models.Dealers;

namespace Pride2.Areas.Admin.ViewModels
{
    public class ShopEditViewModel
    {
        public Shop Shop { get; set; }

        public List<City> Cities { get; set; }

        public int CityId { get; set; }

        public int DealerId { get; set; }

    }
}
