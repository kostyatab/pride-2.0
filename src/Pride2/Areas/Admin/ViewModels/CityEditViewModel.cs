﻿using Pride2.Models.Dealers;

namespace Pride2.Areas.Admin.ViewModels
{
    public class CityEditViewModel
    {
        public City City { get; set; }

        public int RegionId { get; set; }
    }
}
