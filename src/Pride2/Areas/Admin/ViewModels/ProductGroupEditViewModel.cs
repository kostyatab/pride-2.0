﻿using Pride2.Models;

namespace Pride2.Areas.Admin.ViewModels
{
    public class ProductGroupEditViewModel
    {
        public ProductGroup ProductGroup { get; set; }
        public int CategoryId { get; set; }
    }
}
