﻿using System;
using System.ComponentModel.DataAnnotations;
using Pride2.Models;

namespace Pride2.Areas.Admin.ViewModels
{
    public class StockChangeQuantityViewModel
    {
        public Stock Stock { get; set; }

        [Range(0, int.MaxValue)]
        public int Value { get; set; }
        public bool IsPlus { get; set; }
    }
}
