﻿using Pride2.Models;

namespace Pride2.Areas.Admin.ViewModels
{
    public class ProductConfigurationEditViewModel
    {
        public ProductConfiguration ProductConfiguration{ get; set; }
        public int ProductId { get; set; }
    }
}
