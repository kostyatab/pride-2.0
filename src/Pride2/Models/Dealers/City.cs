﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pride2.Models.Dealers
{
    public class City
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual Region Region { get; set; }
        public virtual List<Shop> Shops { get; set; }
    }
}
