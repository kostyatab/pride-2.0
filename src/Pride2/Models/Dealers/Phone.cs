﻿using System.ComponentModel.DataAnnotations;

namespace Pride2.Models.Dealers
{
    public class Phone
    {
        public int Id { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        public Shop Shop { get; set; }
    }
}
