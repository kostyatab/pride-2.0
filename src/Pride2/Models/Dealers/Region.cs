﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pride2.Models.Dealers
{
    public class Region
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual List<City> Cities { get; set; }
    }
}
