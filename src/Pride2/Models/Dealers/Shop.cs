﻿using System.Collections.Generic;

namespace Pride2.Models.Dealers
{
    public class Shop
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }
        public string WorkingTime { get; set; }
        public string CoordX { get; set; }
        public string CoordY { get; set; }

        public virtual Dealer Dealer { get; set; }
        public virtual City City { get; set; }
        public virtual List<Phone> Phones { get; set; }
    }
}
