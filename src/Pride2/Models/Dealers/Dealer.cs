﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pride2.Models.Dealers
{
    public class Dealer
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Surname { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public virtual List<Shop> Shops { get; set; }

    }
}
