﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pride2.Models
{
    public class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Info { get; set; }

        public string Discription { get; set; }

        public string Slug { get; set; }

        [Required]
        [Range(0.01, double.MaxValue)]
        public decimal Price { get; set; }

        public bool IsDiscount { get; set; }

        public decimal DiscountPrice { get; set; }

        public bool IsArchive { get; set; }

        public virtual Category Category { get; set; }
        public virtual ProductGroup ProductGroup { get; set; }
        public virtual List<Stock> Stocks { get; set; }
        public virtual ProductConfiguration ProductConfiguration { get; set; }
    }
}
