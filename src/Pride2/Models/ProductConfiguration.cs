﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pride2.Models
{
    public class ProductConfiguration
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public virtual List<ProductConfigurationItem> ProductConfigurationItem { get; set; }
    }
}
