﻿using System.ComponentModel.DataAnnotations;

namespace Pride2.Models
{
    public class ProductConfigurationItem
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual ProductConfiguration ProductConfiguration { get; set; }
        public virtual Stock Stock { get; set; }
    }
}
