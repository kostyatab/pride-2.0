﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Pride2.Models
{
    public class News
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public string Info { get; set; }

        public DateTime DateTime { get; set; }

        [Required]
        public string ShortArticle { get; set; }

        [Required]
        public string Article { get; set; }

        public string Image { get; set; }

        public int Views { get; set; }
    }
}
