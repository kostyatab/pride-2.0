﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Pride2.Models
{
    public class Media
    {
        public int Id { get; set; }

        [Required]
        public string Link { get; set; }
        public DateTime DateCreate { get; set; }
    }
}
