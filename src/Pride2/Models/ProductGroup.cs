﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pride2.Models
{
    public class ProductGroup
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int Sort { get; set; }

        public virtual Category Category { get; set; }

        public virtual List<Product> Products { get; set; }
    }
}
