﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pride2.Models
{
    public class Category
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Slug { get; set; }

        public virtual List<Product> Products { get; set; }

        public virtual List<ProductGroup> ProductGroup { get; set; }
    }
}
